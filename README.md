This module is intended for ASUS ROG type laptops with the N-Key device to enable all FN+Key combos.

Add to dkms manually with:
1. `dkms add .`
2. `dkms build asus-rog-nb-wmi/1.0.2`
2. `dkms install asus-rog-nb-wmi/1.0.2`

Packaging and auto-builds are available [here](https://build.opensuse.org/package/show/home:luke_nukem:asus/asus-nb-wmi)
Download repositories are available [here](https://download.opensuse.org/repositories/home:/luke_nukem:/asus/)
